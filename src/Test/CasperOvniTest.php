<?php

namespace CasperOvni\Test;

use CasperOvni\CasperOvni;

class CasperOvniTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider cometsAndColors
     */
    public function testNameWillBeTaken($comet, $color, $mustBe)
    {
        $casperOvni = new CasperOvni();
        $this->assertEquals($mustBe, $casperOvni->willBeTaken($comet, $color));
    }

    /**
     * @param $word
     * @param $sum
     * @dataProvider wordsAndSum
     */
    public function testWordMustReturnCorrectSumLetters($word, $sum)
    {
        $casperOvni = new CasperOvni();
        $this->assertEquals($sum, $casperOvni->calculateWord($word));
    }

    public function wordsAndSum()
    {
        return [
            ['a', 1],
            ['ab', 2],
            ['teste', 190000]
        ];
    }

    public function cometsAndColors()
    {
        return [
            ['halley', 'amarelo', false],
            ['encke', 'vermelho', true],
            ['wolf', 'preto', false],
            ['kushida', 'azul', false]
        ];
    }
}
