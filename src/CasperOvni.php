<?php

namespace CasperOvni;

define('DIVISOR', 45);

class CasperOvni
{
    private $words = [
        ['comet' => 'halley', 'group' => 'amarelo'],
        ['comet' => 'encke', 'group' => 'vermelho'],
        ['comet' => 'wolf', 'group' => 'preto'],
        ['comet' => 'kushida', 'group' => 'azul']
    ];

    public function willBeTaken($comet, $color) {
        $cometValue = $this->calculateWord($comet) % DIVISOR;
        $colorValue = $this->calculateWord($color) % DIVISOR;
        if ($cometValue != $colorValue) {
            return true;
        }
        return false;
    }

    private function getLetterValue($letter) {
        $letters = 'abcdefghijklmnopqrstuvwxyz';
        for ($i = 0; $i < strlen($letters); $i++) {
            if ($letter == $letters[$i]) {
                return $i + 1;
            }
        }
    }

    public function calculateWord($word) {
        $word = strtolower($word);
        $total = 1;
        for ($i = 0; $i < strlen($word); $i++) {
            $total *= $this->getLetterValue($word[$i]);
        }
        return $total;
    }

    public function run()
    {
        foreach ($this->words as $word) {
            if ($this->willBeTaken($word['comet'], $word['group'])) {
                echo strtoupper($word['group']) . "\n";
            }
        }
    }
}
