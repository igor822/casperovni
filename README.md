CasperOvni
===========

Projeto consiste no desenvolvimento do teste [Desafio dos OVNIs](https://bitbucket.org/casperlibero/fcl-dev-test/src/3079d4b903fcd0599f988ef7d052762bce9f3bb8/TESTE-3.md?fileviewer=file-view-default)

# Dependência

* PHP 5.5+

# Configurando projeto

### Composer

Ao rodar `composer install` será instalado todas as dependências necessárias para o projeto rodar tranquilamente. .